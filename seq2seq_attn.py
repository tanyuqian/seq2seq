"""Attentional Seq2seq.
"""
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# pylint: disable=invalid-name, too-many-arguments, too-many-locals

import os
import importlib
import tensorflow as tf
import texar as tx
from rouge import Rouge

flags = tf.flags

flags.DEFINE_string("config_model", "config_model", "The model config.")
flags.DEFINE_string("config_data", "config_iwslt14", "The dataset config.")

flags.DEFINE_integer('set_no', 0, '')

FLAGS = flags.FLAGS

dataset = FLAGS.config_data.split('_')[-1]

config_model = importlib.import_module(FLAGS.config_model)
config_data = importlib.import_module(FLAGS.config_data)

set_dir = dataset + '_result_set' + str(FLAGS.set_no) + '/'
log_dir = set_dir + FLAGS.config_data.split('_')[-1] +\
          '_training_log_baseline' + '/'
os.system('mkdir ' + set_dir)
os.system('mkdir ' + log_dir)


def build_model(batch, train_data):
    """Assembles the seq2seq model.
    """
    source_embedder = tx.modules.WordEmbedder(
        vocab_size=train_data.source_vocab.size, hparams=config_model.embedder)

    encoder = tx.modules.BidirectionalRNNEncoder(
        hparams=config_model.encoder)

    enc_outputs, _ = encoder(source_embedder(batch['source_text_ids']))

    target_embedder = tx.modules.WordEmbedder(
        vocab_size=train_data.target_vocab.size, hparams=config_model.embedder)

    decoder = tx.modules.AttentionRNNDecoder(
        memory=tf.concat(enc_outputs, axis=2),
        memory_sequence_length=batch['source_length'],
        vocab_size=train_data.target_vocab.size,
        hparams=config_model.decoder)

    training_outputs, _, _ = decoder(
        decoding_strategy='train_greedy',
        inputs=target_embedder(batch['target_text_ids'][:, :-1]),
        sequence_length=batch['target_length'] - 1)

    train_op = tx.core.get_train_op(
        tx.losses.sequence_sparse_softmax_cross_entropy(
            labels=batch['target_text_ids'][:, 1:],
            logits=training_outputs.logits,
            sequence_length=batch['target_length'] - 1))

    start_tokens = tf.ones_like(batch['target_length']) *\
                   train_data.target_vocab.bos_token_id
    beam_search_outputs, _, _ = \
        tx.modules.beam_search_decode(
            decoder_or_cell=decoder,
            embedding=target_embedder,
            start_tokens=start_tokens,
            end_token=train_data.target_vocab.eos_token_id,
            beam_width=config_model.beam_width,
            max_decoding_length=60)

    return train_op, beam_search_outputs


def main():
    """Entrypoint.
    """
    train_data = tx.data.PairedTextData(hparams=config_data.train)
    val_data = tx.data.PairedTextData(hparams=config_data.val)
    test_data = tx.data.PairedTextData(hparams=config_data.test)
    data_iterator = tx.data.TrainTestDataIterator(
        train=train_data, val=val_data, test=test_data)

    batch = data_iterator.get_next()

    train_op, infer_outputs = build_model(batch, train_data)

    def _train_epoch(sess, epoch_no):
        data_iterator.switch_to_train_data(sess)
        training_log_file = \
            open(log_dir + 'training_log' + str(epoch_no) + '.txt', 'w')

        step = 0
        while True:
            try:
                loss = sess.run(train_op)
                print("step={}, loss={:.4f}".format(step, loss),
                      file=training_log_file)
                training_log_file.flush()
                step += 1
            except tf.errors.OutOfRangeError:
                break

    def _eval_epoch(sess, mode, epoch_no):
        if mode == 'val':
            data_iterator.switch_to_val_data(sess)
        else:
            data_iterator.switch_to_test_data(sess)

        output_file = \
            open(log_dir + mode + '_results' + str(epoch_no) + '.txt', 'w')

        refs, hypos = [], []
        while True:
            try:
                fetches = [
                    batch['target_text'][:, 1:],
                    infer_outputs.predicted_ids[:, :, 0]
                ]
                feed_dict = {
                    tx.global_mode(): tf.estimator.ModeKeys.EVAL
                }
                target_texts_ori, output_ids = \
                    sess.run(fetches, feed_dict=feed_dict)

                target_texts = \
                    [s[:s.index('<EOS>')] for s in target_texts_ori.tolist()]
                output_texts = tx.utils.map_ids_to_strs(
                    ids=output_ids, vocab=val_data.target_vocab)

                for hypo in output_texts:
                    print(hypo.encode('utf-8'), file=output_file)
                output_file.flush()

                for hypo, ref in zip(output_texts, target_texts):
                    if dataset == 'iwslt14':
                        hypos.append(hypo)
                        refs.append([ref])
                    elif dataset == 'giga':
                        hypos.append(hypo.decode('utf-8'))
                        refs.append(' '.join(ref).decode('utf-8'))
            except tf.errors.OutOfRangeError:
                break

        if dataset == 'iwslt14':
            return tx.evals.corpus_bleu_moses(
                list_of_references=refs, hypotheses=hypos)
        elif dataset == 'giga':
            rouge = Rouge()
            return rouge.get_scores(hyps=hypos, refs=refs, avg=True)

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        sess.run(tf.local_variables_initializer())
        sess.run(tf.tables_initializer())

        best_val_score = -1.
        scores_file = open(log_dir + 'scores.txt', 'w')
        for i in range(config_data.num_epochs):
            _train_epoch(sess, i)

            val_score = _eval_epoch(sess, 'val', i)
            test_score = _eval_epoch(sess, 'test', i)

            def _calc_reward(score):
                if dataset == 'iwslt14':
                    return score
                else:
                    return sum([value['f'] for key, value in score.items()])

            best_val_score = max(best_val_score, _calc_reward(val_score))

            if dataset == 'iwslt14':
                print('val epoch={}, BLEU={:.4f}; best-ever={:.4f}'.format(
                    i, val_score, best_val_score), file=scores_file)

                print('test epoch={}, BLEU={:.4f}'.format(i, test_score),
                      file=scores_file)
                print('=' * 50, file=scores_file)

            elif dataset == 'giga':
                print('valid epoch', i, ':', file=scores_file)
                for key, value in val_score.items():
                    print(key, value, file=scores_file)
                print('fsum:', _calc_reward(val_score),
                      'best_val_fsum:', best_val_score, file=scores_file)

                print('test epoch', i, ':', file=scores_file)
                for key, value in test_score.items():
                    print(key, value, file=scores_file)
                print('=' * 110, file=scores_file)

            scores_file.flush()


if __name__ == '__main__':
    main()

