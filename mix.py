"""Attentional Seq2seq.
"""
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import importlib
import os

import tensorflow as tf
import texar as tx
import numpy as np

from mixsample_decoder import MixSampleRNNDecoder
from rouge import Rouge

flags = tf.flags

flags.DEFINE_string("config_model", "config_model", "The model config.")
flags.DEFINE_string("config_data", "config_iwslt14", "The dataset config.")

flags.DEFINE_integer('set_no', 0, '')

flags.DEFINE_string('init', '[0.01,0.99,0.]', '')

flags.DEFINE_float('delta_rew', 0.0, '')
flags.DEFINE_float('delta_self', 0.0, '')
flags.DEFINE_float('w', 1., '')
flags.DEFINE_string('mode', 'alter', '')
flags.DEFINE_integer('k', 2, '')

FLAGS = flags.FLAGS

dataset = FLAGS.config_data.split('_')[-1]

config_model = importlib.import_module(FLAGS.config_model)
config_data = importlib.import_module(FLAGS.config_data)

FLAGS.init = eval(FLAGS.init)

set_dir = dataset + '_result_set' + str(FLAGS.set_no) + '/'
log_dir = set_dir + 'training_log_mixsample' +\
          '_init' + '_' + str(FLAGS.init[0]) +\
          '_' + str(FLAGS.init[1]) +\
          '_' + str(FLAGS.init[2]) +\
          '_w' + str(FLAGS.w) +\
          '_' + FLAGS.mode +\
          '_dr' + str(FLAGS.delta_rew) +\
          '_ds' + str(FLAGS.delta_self) +\
          '_k' + str(FLAGS.k) + '/'
os.system('mkdir ' + set_dir)
os.system('mkdir ' + log_dir)


def build_model(data_batch, source_vocab, target_vocab, lambdas):
    """Assembles the seq2seq model.
    """
    batch_size = tf.shape(data_batch['target_length'])[0]
    source_vocab_size = source_vocab.size
    target_vocab_size = target_vocab.size
    target_bos_token_id = target_vocab.bos_token_id
    target_eos_token_id = target_vocab.eos_token_id

    source_embedder = tx.modules.WordEmbedder(
        vocab_size=source_vocab_size, hparams=config_model.embedder)

    encoder = tx.modules.BidirectionalRNNEncoder(
        hparams=config_model.encoder)

    enc_outputs, _ = encoder(source_embedder(data_batch['source_text_ids']))

    embedder = tx.modules.WordEmbedder(
        vocab_size=target_vocab_size, hparams=config_model.embedder)

    reward_metric = 'bleu' if dataset == 'iwslt14' else 'rouge'
    decoder = MixSampleRNNDecoder(
        reward_metric=reward_metric,
        w=FLAGS.w,
        memory=tf.concat(enc_outputs, axis=2),
        memory_sequence_length=data_batch['source_length'],
        vocab=target_vocab,
        ground_truth=data_batch['target_text_ids'][:, 1:],
        ground_truth_length=data_batch['target_length'] - 1,
        vocab_size=target_vocab_size,
        lambdas=lambdas,
        hparams=config_model.decoder)

    start_tokens = \
        tf.ones_like(data_batch['target_length']) * target_bos_token_id
    training_outputs, _, training_length = decoder(
        decoding_strategy='infer_sample',
        initial_state=decoder.zero_state(
            batch_size=batch_size, dtype=tf.float32),
        max_decoding_length=60,
        embedding=embedder,
        start_tokens=start_tokens,
        end_token=target_eos_token_id,
        sequence_length=data_batch['target_length'] - 1)

    train_op = tx.core.get_train_op(
        tx.losses.sequence_sparse_softmax_cross_entropy(
            labels=training_outputs.sample_id,
            logits=training_outputs.logits,
            sequence_length=training_length))

    start_tokens = \
        tf.ones_like(data_batch['target_length']) * target_bos_token_id
    beam_search_outputs, _, _ = \
        tx.modules.beam_search_decode(
            decoder_or_cell=decoder,
            embedding=embedder,
            start_tokens=start_tokens,
            end_token=target_eos_token_id,
            beam_width=config_model.beam_width,
            max_decoding_length=60)

    return train_op, training_outputs, beam_search_outputs


def main():
    """Entrypoint.
    """
    training_data = tx.data.PairedTextData(hparams=config_data.train)
    val_data = tx.data.PairedTextData(hparams=config_data.val)
    test_data = tx.data.PairedTextData(hparams=config_data.test)
    data_iterator = tx.data.TrainTestDataIterator(
        train=training_data, val=val_data, test=test_data)

    source_vocab = training_data.source_vocab
    target_vocab = training_data.target_vocab

    data_batch = data_iterator.get_next()
    lambdas_ts = tf.placeholder(shape=[3,], dtype=tf.float32)

    train_op, training_outputs, infer_outputs = build_model(
        data_batch, source_vocab, target_vocab, lambdas_ts)

    def _train_epoch(sess, epoch, lambdas):
        data_iterator.switch_to_train_data(sess)
        log_file = open(log_dir + 'training_log' + str(epoch) + '.txt', 'w')

        step = 0
        while True:
            try:
                assert min(lambdas) > -1e-5
                loss = sess.run(train_op, feed_dict={
                    lambdas_ts: np.array(lambdas)})
                print("step={}, loss={:.4f}, lambdas={}".format(
                    step, loss, lambdas), file=log_file)
                log_file.flush()
                step += 1

            except tf.errors.OutOfRangeError:
                break

    def _eval_epoch(sess, mode, epoch_no):
        if mode == 'val':
            data_iterator.switch_to_val_data(sess)
        else:
            data_iterator.switch_to_test_data(sess)

        output_file = \
            open(log_dir + mode + '_results' + str(epoch_no) + '.txt', 'w')

        refs, hypos = [], []
        while True:
            try:
                fetches = [
                    data_batch['target_text'][:, 1:],
                    infer_outputs.predicted_ids[:, :, 0]
                ]
                feed_dict = {
                    tx.global_mode(): tf.estimator.ModeKeys.PREDICT
                }
                target_texts_ori, output_ids = \
                    sess.run(fetches, feed_dict=feed_dict)

                target_texts = \
                    [s[:s.index('<EOS>')] for s in target_texts_ori.tolist()]
                output_texts = tx.utils.map_ids_to_strs(
                    ids=output_ids, vocab=val_data.target_vocab)

                for hypo in output_texts:
                    print(hypo.encode('utf-8'), file=output_file)
                output_file.flush()

                for hypo, ref in zip(output_texts, target_texts):
                    if dataset == 'iwslt14':
                        hypos.append(hypo)
                        refs.append([ref])
                    elif dataset == 'giga':
                        hypos.append(hypo.decode('utf-8'))
                        refs.append(' '.join(ref).decode('utf-8'))
            except tf.errors.OutOfRangeError:
                break

        if dataset == 'iwslt14':
            return tx.evals.corpus_bleu_moses(
                list_of_references=refs, hypotheses=hypos)
        elif dataset == 'giga':
            rouge = Rouge()
            return rouge.get_scores(hyps=hypos, refs=refs, avg=True)

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        sess.run(tf.local_variables_initializer())
        sess.run(tf.tables_initializer())

        lambdas = FLAGS.init

        best_val_score, best_tmp = -1., -1.
        updates = ['rew'] * FLAGS.k
        scores_file = open(log_dir + 'scores.txt', 'w')
        for i in range(config_data.num_epochs_mix):
            print('training epoch={}, lambdas={}'.format(i, lambdas),
                  file=scores_file)
            _train_epoch(sess, i, lambdas)

            val_score = _eval_epoch(sess, 'val', i)
            test_score = _eval_epoch(sess, 'test', i)

            def _update_self():
                while min(lambdas[1], 1. - lambdas[0]) < FLAGS.delta_self:
                    FLAGS.delta_self /= 2.
                lambdas[1] -= FLAGS.delta_self
                lambdas[0] += FLAGS.delta_self
                updates.append('self')

            def _update_rew():
                while min(lambdas[1], 1. - lambdas[2]) < FLAGS.delta_rew:
                    FLAGS.delta_rew /= 2.
                lambdas[1] -= FLAGS.delta_rew
                lambdas[2] += FLAGS.delta_rew
                updates.append('rew')

            def _calc_reward(score):
                if dataset == 'iwslt14':
                    return score
                else:
                    return sum([value['f'] for key, value in score.items()])

            if _calc_reward(val_score) < best_tmp:
                if FLAGS.mode == 'alter':
                    if updates[-FLAGS.k:] == ['rew'] * FLAGS.k:
                        _update_self()
                    else:
                        _update_rew()
                elif FLAGS.mode == 'self_only':
                    _update_self()
                elif FLAGS.mode == 'rew_only':
                    _update_rew()
                best_tmp = -1.
            else:
                best_tmp = _calc_reward(val_score)

            best_val_score = max(best_val_score, _calc_reward(val_score))

            if dataset == 'iwslt14':
                print('val epoch={}, BLEU={:.4f}; best-ever={:.4f}'.format(
                    i, val_score, best_val_score), file=scores_file)

                print('test epoch={}, BLEU={:.4f}'.format(i, test_score),
                      file=scores_file)
                print('=' * 50, file=scores_file)

            elif dataset == 'giga':
                print('valid epoch', i, ':', file=scores_file)
                for key, value in val_score.items():
                    print(key, value, file=scores_file)
                print('fsum:', _calc_reward(val_score),
                      'best_val_fsum:', best_val_score, file=scores_file)

                print('test epoch', i, ':', file=scores_file)
                for key, value in test_score.items():
                    print(key, value, file=scores_file)
                print('=' * 110, file=scores_file)

            scores_file.flush()


if __name__ == '__main__':
    main()
