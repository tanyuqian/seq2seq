from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

# pylint: disable=no-name-in-module, too-many-arguments, too-many-locals
# pylint: disable=not-context-manager, protected-access, invalid-name

import tensorflow as tf
import numpy as np

from tensorflow.python.util import nest

from texar.modules.decoders.rnn_decoders import \
    AttentionRNNDecoder, AttentionRNNDecoderOutput


class MixerDecoder(AttentionRNNDecoder):
    def __init__(self,
                 vocab,
                 ground_truth,
                 ground_truth_length,
                 truth_feeding_length,
                 memory,
                 memory_sequence_length=None,
                 cell=None,
                 cell_dropout_mode=None,
                 vocab_size=None,
                 output_layer=None,
                 cell_input_fn=None,
                 hparams=None):
        AttentionRNNDecoder.__init__(
            self, memory, memory_sequence_length, cell, cell_dropout_mode,
            vocab_size, output_layer, cell_input_fn, hparams)

        self._vocab = vocab
        self._ground_truth = ground_truth
        self._ground_truth_length = ground_truth_length
        self._truth_feeding_length = truth_feeding_length

    def step(self, time, inputs, state, name=None):
        wrapper_outputs, wrapper_state = self._cell(inputs, state)
        logits = self._output_layer(wrapper_outputs)

        truth_feeding = lambda : tf.cond(
            tf.less(time, tf.shape(self._ground_truth)[1]),
            lambda : tf.to_int32(self._ground_truth[:, time]),
            lambda : tf.ones_like(self._ground_truth[:, 0],
                                  dtype=tf.int32) * self._vocab.eos_token_id)

        self_feeding = lambda : self._helper.sample(
                time=time, outputs=logits, state=wrapper_state)

        sample_ids = tf.cond(
            tf.less(time, self._truth_feeding_length),
            truth_feeding, self_feeding)

        (finished, next_inputs, next_state) = self._helper.next_inputs(
            time=time,
            outputs=logits,
            state=wrapper_state,
            sample_ids=sample_ids)

        attention_scores = wrapper_state.alignments
        attention_context = wrapper_state.attention
        outputs = AttentionRNNDecoderOutput(
            logits, sample_ids, wrapper_outputs,
            attention_scores, attention_context)

        return (outputs, next_state, next_inputs, finished)
