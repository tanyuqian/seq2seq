from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

# pylint: disable=no-name-in-module, too-many-arguments, too-many-locals
# pylint: disable=not-context-manager, protected-access, invalid-name

import tensorflow as tf
import numpy as np

from tensorflow.python.util import nest

from texar.modules.decoders.rnn_decoders import \
    AttentionRNNDecoder, AttentionRNNDecoderOutput

from texar.evals.bleu import sentence_bleu
from rouge import Rouge

rouge = Rouge()


def calc_reward(time, refs, hypo, unk_id, metric, w):
    if len(hypo) == 0 or len(refs[0]) == 0:
        return 0.

    for i in range(len(hypo)):
        assert isinstance(hypo[i], int)
        if hypo[i] == unk_id:
            hypo[i] = -1

    if metric == 'bleu':
        return w * sentence_bleu(references=refs, hypothesis=hypo, smooth=True)
    else:
        ref_str = ' '.join([str(word) for word in refs[0]])
        hypo_str = ' '.join([str(word) for word in hypo])
        rouge_scores = \
            rouge.get_scores(hyps=[hypo_str], refs=[ref_str], avg=True)
        return w * sum([value['f'] for key, value in rouge_scores.items()])


class MixSampleRNNDecoder(AttentionRNNDecoder):
    def __init__(self,
                 vocab,
                 reward_metric,
                 w,
                 ground_truth,
                 ground_truth_length,
                 lambdas,
                 memory,
                 memory_sequence_length=None,
                 cell=None,
                 cell_dropout_mode=None,
                 vocab_size=None,
                 output_layer=None,
                 cell_input_fn=None,
                 hparams=None):
        AttentionRNNDecoder.__init__(
            self, memory, memory_sequence_length, cell, cell_dropout_mode,
            vocab_size, output_layer, cell_input_fn, hparams)

        self._vocab = vocab
        self._ground_truth = ground_truth
        self._lambdas = lambdas
        self._ground_truth_length = ground_truth_length
        self._metric = reward_metric
        self._w = w

    def initialize(self, name=None):
        helper_init = self._helper.initialize()

        flat_initial_state = nest.flatten(self._initial_state)
        dtype = flat_initial_state[0].dtype
        initial_state = self._cell.zero_state(
            batch_size=tf.shape(flat_initial_state[0])[0], dtype=dtype)
        initial_state = initial_state.clone(cell_state=self._initial_state)

        initial_state = [tf.ones((
            tf.shape(flat_initial_state[0])[0], 60),
            dtype=tf.int32) * self._vocab.eos_token_id, initial_state]

        return [helper_init[0], helper_init[1], initial_state]

    def step(self, time, inputs, state, name=None):
        wrapper_outputs, wrapper_state = self._cell(inputs, state[1])
        logits = self._output_layer(wrapper_outputs)

        sample_method_sampler = \
            tf.distributions.Categorical(probs=self._lambdas)
        sample_method_id = sample_method_sampler.sample()

        def _rewards_sample_ids():
            def _get_rewards(time, prefix_ids, target_ids, ground_truth_length):
                batch_size = np.shape(target_ids)[0]
                words_in_target = \
                    [np.unique(target_ids[i]) for i in range(batch_size)]
                unk_id = self._vocab.unk_token_id
                eos_id = self._vocab.eos_token_id

                # before append
                baseline_scores = []
                baseline_ids = prefix_ids[:, :time]
                for i in range(batch_size):
                    ref = target_ids[i].tolist()
                    if self._vocab.eos_token_id in ref:
                        ref = ref[:ref.index(self._vocab.eos_token_id)]

                    hypo = baseline_ids[i].tolist()
                    if self._vocab.eos_token_id in hypo:
                        hypo = hypo[:hypo.index(self._vocab.eos_token_id)]

                    baseline_scores.append(calc_reward(
                        time=time, refs=[ref], hypo=hypo, unk_id=unk_id,
                        metric=self._metric, w=self._w))

                # append UNK
                syn_ids = np.concatenate([
                    prefix_ids[:, :time],
                    np.ones((batch_size, 1), dtype=np.int32) * unk_id], axis=1)

                reward_unk = []
                for i in range(batch_size):
                    ref = target_ids[i].tolist()
                    if self._vocab.eos_token_id in ref:
                        ref = ref[:ref.index(self._vocab.eos_token_id)]

                    hypo = syn_ids[i].tolist()
                    if self._vocab.eos_token_id in hypo:
                        hypo = hypo[:hypo.index(self._vocab.eos_token_id)]

                    reward = calc_reward(time=time, refs=[ref], hypo=hypo,
                                         unk_id=unk_id, metric=self._metric, w=self._w)
                    reward_unk.append(
                        np.ones((1, self._vocab_size), dtype=np.float32) *
                        reward - baseline_scores[i])
                result = np.concatenate(reward_unk, axis=0)

                # append tokens
                for i in range(batch_size):
                    for id in words_in_target[i]:
                        if id == unk_id:
                            continue

                        syn_id = np.concatenate(
                            [prefix_ids[i:i + 1, :time], np.array([[id, ]])],
                            axis=1)
                        hypo = syn_id[0].tolist()
                        if self._vocab.eos_token_id in hypo:
                            hypo = hypo[:hypo.index(self._vocab.eos_token_id)]

                        ref = target_ids[i].tolist()
                        if self._vocab.eos_token_id in ref:
                            ref = ref[:ref.index(self._vocab.eos_token_id)]

                        dup = 1. if prefix_ids[i][time] == id and \
                                    id != unk_id else 0.
                        eos = 1. if time < ground_truth_length[i] - 1 and \
                                    id == eos_id else 0.

                        reward = calc_reward(
                            time=time, refs=[ref], hypo=hypo, unk_id=unk_id,
                            metric=self._metric, w=self._w)
                        result[i][id] = reward - baseline_scores[i] - dup - eos

                return result

            sampler = tf.distributions.Categorical(
                logits=tf.py_func(_get_rewards, [
                    time, state[0], self._ground_truth,
                    self._ground_truth_length], tf.float32))
            return tf.reshape(
                sampler.sample(), (tf.shape(self._ground_truth)[0],))

        truth_feeding = lambda : tf.cond(
            tf.less(time, tf.shape(self._ground_truth)[1]),
            lambda : tf.to_int32(self._ground_truth[:, time]),
            lambda : tf.ones_like(self._ground_truth[:, 0],
                                  dtype=tf.int32) * self._vocab.eos_token_id)

        self_feeding = lambda : self._helper.sample(
                time=time, outputs=logits, state=wrapper_state)

        reward_feeding = _rewards_sample_ids

        sample_ids = tf.cond(
            tf.logical_or(tf.equal(time, 0), tf.equal(sample_method_id, 1)),
            truth_feeding,
            lambda : tf.cond(
                tf.equal(sample_method_id, 2),
                reward_feeding,
                self_feeding))

        (finished, next_inputs, next_state) = self._helper.next_inputs(
            time=time,
            outputs=logits,
            state=wrapper_state,
            sample_ids=sample_ids)

        next_state = [tf.concat(
            [state[0][:, :time], tf.expand_dims(sample_ids, 1),
             state[0][:, time + 1:]], axis=1), next_state]
        next_state[0] = tf.reshape(next_state[0], (tf.shape(inputs)[0], 60))

        attention_scores = wrapper_state.alignments
        attention_context = wrapper_state.attention
        outputs = AttentionRNNDecoderOutput(
            logits, sample_ids, wrapper_outputs,
            attention_scores, attention_context)

        return (outputs, next_state, next_inputs, finished)
