"""Attentional Seq2seq.
"""
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# pylint: disable=invalid-name, too-many-arguments, too-many-locals

import os
import importlib
import tensorflow as tf
import texar as tx
import numpy as np

from mixer_decoder import MixerDecoder
from texar.evals.bleu import sentence_bleu
from rouge import Rouge

rouge = Rouge()

flags = tf.flags

flags.DEFINE_string("config_model", "config_model", "The model config.")
flags.DEFINE_string("config_data", "config_iwslt14", "The dataset config.")

flags.DEFINE_integer('set_no', 0, '')

flags.DEFINE_integer('pretrain_epochs', 10, '')

flags.DEFINE_integer('l', 50, '')
flags.DEFINE_integer('delta', 3, '')
flags.DEFINE_integer('n', 3, '')

FLAGS = flags.FLAGS

dataset = FLAGS.config_data.split('_')[-1]

config_model = importlib.import_module(FLAGS.config_model)
config_data = importlib.import_module(FLAGS.config_data)

set_dir = dataset + '_result_set' + str(FLAGS.set_no) + '/'
log_dir = set_dir + 'training' +\
          '_pt' + str(FLAGS.pretrain_epochs) +\
          '_l' + str(FLAGS.l) +\
          '_delta' + str(FLAGS.delta) +\
          '_n' + str(FLAGS.n) + '/'
os.system('mkdir ' + set_dir)
os.system('mkdir ' + log_dir)


def build_model(batch, train_data, truth_feeding_length):
    """Assembles the seq2seq model.
    """
    batch_size = tf.shape(batch['source_length'])[0]

    source_embedder = tx.modules.WordEmbedder(
        vocab_size=train_data.source_vocab.size, hparams=config_model.embedder)

    encoder = tx.modules.BidirectionalRNNEncoder(
        hparams=config_model.encoder)

    enc_outputs, _ = encoder(source_embedder(batch['source_text_ids']))

    target_embedder = tx.modules.WordEmbedder(
        vocab_size=train_data.target_vocab.size, hparams=config_model.embedder)

    decoder = MixerDecoder(
        vocab=train_data.target_vocab,
        ground_truth=batch['target_text_ids'][:, 1:],
        ground_truth_length=batch['target_length'] - 1,
        truth_feeding_length=truth_feeding_length,
        memory=tf.concat(enc_outputs, axis=2),
        memory_sequence_length=batch['source_length'],
        vocab_size=train_data.target_vocab.size,
        hparams=config_model.decoder)

    start_tokens = tf.ones_like(batch['target_length']) *\
                   train_data.target_vocab.bos_token_id
    training_outputs, _, training_length = decoder(
        decoding_strategy='infer_sample',
        initial_state=decoder.zero_state(
            batch_size=batch_size, dtype=tf.float32),
        max_decoding_length=tf.reduce_max(batch['target_length'] - 1),
        embedding=target_embedder,
        start_tokens=start_tokens,
        end_token=train_data.target_vocab.eos_token_id)

    mle_loss = tx.losses.sequence_sparse_softmax_cross_entropy(
            labels=batch['target_text_ids'][:, 1:truth_feeding_length + 1],
            logits=training_outputs.logits[:, :truth_feeding_length],
            sequence_length=tf.minimum(
                batch['target_length'] - 1, truth_feeding_length))

    def _get_reward(refs, hypos):
        def _calc(refs, hypo, unk_id):
            if len(hypo) == 0 or len(refs[0]) == 0:
                return 0.

            for i in range(len(hypo)):
                assert isinstance(hypo[i], int)
                if hypo[i] == unk_id:
                    hypo[i] = -1

            if dataset == 'iwslt14':
                return 0.01 * sentence_bleu(
                    references=refs, hypothesis=hypo, smooth=True)
            elif dataset == 'giga':
                ref_str = ' '.join([str(word) for word in refs[0]])
                hypo_str = ' '.join([str(word) for word in hypo])
                rouge_scores = \
                    rouge.get_scores(hyps=[hypo_str], refs=[ref_str], avg=True)
                return sum([value['f'] for key, value in rouge_scores.items()])
            else:
                raise ValueError

        result = []
        for i in range(refs.shape[0]):
            ref = refs[i].tolist()
            hypo = hypos[i].tolist()
            result.append(_calc(
                refs=[ref], hypo=hypo,
                unk_id=train_data.target_vocab.unk_token_id))

        return np.array(result, dtype=np.float32)

    advantages = tf.py_func(
        _get_reward,
        [batch['target_text_ids'][:, 1:], training_outputs.sample_id],
        tf.float32) - tf.py_func(
        _get_reward,
        [batch['target_text_ids'][:, 1:],
         training_outputs.sample_id[:, truth_feeding_length:]],
        tf.float32)

    reinforce_loss = tx.losses.sequence_sparse_softmax_cross_entropy(
        labels=training_outputs.sample_id[:, truth_feeding_length:],
        logits=training_outputs.logits[:, truth_feeding_length:],
        sequence_length=tf.maximum(
            0, tf.minimum(training_length, batch['target_length'] - 1) -
               truth_feeding_length),
        average_across_batch=False) * advantages
    reinforce_loss = tf.reduce_mean(reinforce_loss)

    train_op = tx.core.get_train_op(mle_loss + reinforce_loss)

    start_tokens = tf.ones_like(batch['target_length']) *\
                   train_data.target_vocab.bos_token_id
    beam_search_outputs, _, _ = \
        tx.modules.beam_search_decode(
            decoder_or_cell=decoder,
            embedding=target_embedder,
            start_tokens=start_tokens,
            end_token=train_data.target_vocab.eos_token_id,
            beam_width=config_model.beam_width,
            max_decoding_length=60)

    return train_op, beam_search_outputs


def main():
    """Entrypoint.
    """
    train_data = tx.data.PairedTextData(hparams=config_data.train)
    val_data = tx.data.PairedTextData(hparams=config_data.val)
    test_data = tx.data.PairedTextData(hparams=config_data.test)
    data_iterator = tx.data.TrainTestDataIterator(
        train=train_data, val=val_data, test=test_data)

    batch = data_iterator.get_next()
    truth_feeding_length_ts = tf.placeholder(shape=[], dtype=tf.int32)

    train_op, infer_outputs = \
        build_model(batch, train_data, truth_feeding_length_ts)

    def _train_epoch(sess, epoch_no, truth_len):
        data_iterator.switch_to_train_data(sess)
        training_log_file = \
            open(log_dir + 'training_log' + str(epoch_no) + '.txt', 'w')

        step = 0
        while True:
            try:
                loss = sess.run(
                    train_op, feed_dict={truth_feeding_length_ts: truth_len})
                print("step={}, loss={:.4f}".format(step, loss),
                      file=training_log_file)
                training_log_file.flush()
                step += 1
            except tf.errors.OutOfRangeError:
                break

    def _eval_epoch(sess, mode):
        if mode == 'val':
            data_iterator.switch_to_val_data(sess)
        else:
            data_iterator.switch_to_test_data(sess)

        refs, hypos = [], []
        while True:
            try:
                fetches = [
                    batch['target_text'][:, 1:],
                    infer_outputs.predicted_ids[:, :, 0]
                ]
                feed_dict = {
                    tx.global_mode(): tf.estimator.ModeKeys.EVAL
                }
                target_texts_ori, output_ids = \
                    sess.run(fetches, feed_dict=feed_dict)

                target_texts = \
                    [s[:s.index('<EOS>')] for s in target_texts_ori.tolist()]
                output_texts = tx.utils.map_ids_to_strs(
                    ids=output_ids, vocab=val_data.target_vocab)

                for hypo, ref in zip(output_texts, target_texts):
                    if dataset == 'iwslt14':
                        hypos.append(hypo)
                        refs.append([ref])
                    elif dataset == 'giga':
                        hypos.append(hypo.decode('utf-8'))
                        refs.append(' '.join(ref).decode('utf-8'))
            except tf.errors.OutOfRangeError:
                break

        if dataset == 'iwslt14':
            return tx.evals.corpus_bleu_moses(
                list_of_references=refs, hypotheses=hypos)
        elif dataset == 'giga':
            rouge = Rouge()
            return rouge.get_scores(hyps=hypos, refs=refs, avg=True)

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        sess.run(tf.local_variables_initializer())
        sess.run(tf.tables_initializer())

        best_val_score = -1.
        scores_file = open(log_dir + 'scores.txt', 'w')
        print('=' * 10, 'pretrain', '=' * 10, file=scores_file)
        for i in range(FLAGS.pretrain_epochs):
            _train_epoch(sess, i, 10000)

            val_score = _eval_epoch(sess, 'val')
            test_score = _eval_epoch(sess, 'test')

            def _calc_reward(score):
                if dataset == 'iwslt14':
                    return score
                else:
                    return sum([value['f'] for key, value in score.items()])

            best_val_score = max(best_val_score, _calc_reward(val_score))

            if dataset == 'iwslt14':
                print('val epoch={}, BLEU={:.4f}; best-ever={:.4f}'.format(
                    i, val_score, best_val_score), file=scores_file)

                print('test epoch={}, BLEU={:.4f}'.format(i, test_score),
                      file=scores_file)
                print('=' * 50, file=scores_file)

            elif dataset == 'giga':
                print('valid epoch', i, ':', file=scores_file)
                for key, value in val_score.items():
                    print(key, value, file=scores_file)
                print('fsum:', _calc_reward(val_score),
                      'best_val_fsum:', best_val_score, file=scores_file)

                print('test epoch', i, ':', file=scores_file)
                for key, value in test_score.items():
                    print(key, value, file=scores_file)
                print('=' * 110, file=scores_file)

            scores_file.flush()

        for len in range(FLAGS.l, -1, -FLAGS.delta):
            print('=' * 10, 'len:', len, '=' * 10, file=scores_file)
            for i in range(FLAGS.n):
                _train_epoch(sess, i, len)

                val_score = _eval_epoch(sess, 'val')
                test_score = _eval_epoch(sess, 'test')

                def _calc_reward(score):
                    if dataset == 'iwslt14':
                        return score
                    else:
                        return sum([value['f'] for key, value in score.items()])

                best_val_score = max(best_val_score, _calc_reward(val_score))

                if dataset == 'iwslt14':
                    print('val epoch={}, BLEU={:.4f}; best-ever={:.4f}'.format(
                        i, val_score, best_val_score), file=scores_file)

                    print('test epoch={}, BLEU={:.4f}'.format(i, test_score),
                          file=scores_file)
                    print('=' * 50, file=scores_file)

                elif dataset == 'giga':
                    print('valid epoch', i, ':', file=scores_file)
                    for key, value in val_score.items():
                        print(key, value, file=scores_file)
                    print('fsum:', _calc_reward(val_score),
                          'best_val_fsum:', best_val_score, file=scores_file)

                    print('test epoch', i, ':', file=scores_file)
                    for key, value in test_score.items():
                        print(key, value, file=scores_file)
                    print('=' * 110, file=scores_file)

                scores_file.flush()


if __name__ == '__main__':
    main()

